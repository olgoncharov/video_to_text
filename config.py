import os


BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class Configuration(object):
    DEBUG = True
    UPLOAD_FOLDER = os.path.join(BASE_DIR, 'uploads')
