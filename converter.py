from math import ceil
import os
import tempfile

from dotenv import load_dotenv
from moviepy.editor import AudioFileClip
import requests

load_dotenv()
YA_TOKEN = os.getenv('YA_TOKEN')
FOLDER_ID = os.getenv('FOLDER_ID')


class Converter(object):

    def __init__(self, filename):
        self.filename = filename

    def extract_audio(self, fragment_size=20):
        fragments = []

        audio_clip = AudioFileClip(self.filename, fps=8000)
        count_of_fragments = ceil(audio_clip.duration / fragment_size)

        for i in range(count_of_fragments):
            t_start = i * fragment_size
            t_end = min(t_start + fragment_size, audio_clip.duration)
            audio_subclip = audio_clip.subclip(t_start, t_end)

            with tempfile.NamedTemporaryFile(suffix='.ogg') as fp:
                audio_subclip.write_audiofile(fp.name, logger=None)
                fragments.append(fp.read())

        return fragments

    def get_text(self):
        headers = {
            'Authorization': f'Bearer {YA_TOKEN}',
        }
        params = {
            'folderId': FOLDER_ID,
            'lang': 'ru-RU'
        }
        try:
            audio_fragments = self.extract_audio()
            text_fragments = []

            for audio_content in audio_fragments:
                response = requests.post(
                    'https://stt.api.cloud.yandex.net/speech/v1/stt:recognize',
                    data=audio_content,
                    headers=headers,
                    params=params
                )
                data = response.json()

                error_message = data.get('error_message')
                if error_message is None:
                    text_fragments.append(data['result'])
                else:
                    raise Exception(error_message)

            return ' '.join(text_fragments)

        except Exception as err:
            print(err)
            return 'Не удалось распознать текст'
