import os

from flask import render_template, request
from werkzeug.utils import secure_filename

from app import app
from converter import Converter


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        f = request.files['file']
        filename = os.path.join(
            app.config['UPLOAD_FOLDER'],
            secure_filename(f.filename)
        )
        f.save(filename)
        converter = Converter(filename)
        text = converter.get_text()
        return render_template(
            'index.html',
            extracted_text=text
        )

    return render_template('index.html')
